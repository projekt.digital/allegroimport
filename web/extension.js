jQuery(document).ready(function() {
  jQuery('form.allegro-import-form').on('submit', function() {
    var form = jQuery(this);
    var actionUrl = form.prop('action');
    var submit = form.find('input[type="submit"]');

    submit.prop('disabled', true);
    
    jQuery.ajax({
      type: 'POST',
      url: actionUrl,
      data: form.serialize()
    }).done(function(response) {
      submit.prop('disabled', false);
      console.log(response);
      if (response.response === 200) {

      } else if (response.response === 400) {
        
      }
    })

    return false;
  })
});