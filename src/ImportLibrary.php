<?php
namespace Bolt\Extension\ProjektDigital\AllegroImport;

use PDO;
use Cocur\Slugify\Slugify;
use Symfony\Component\HttpFoundation\Response;

class ImportLibrary
{
	private $export = [];
	private $item = [];
	private $file;
	private $item_template;
	private $microTime;
	private $sqliteDB;
	public $message;

	public $response;

	private $slugify;
	public

	function __construct($file = null)
	{
		if ($file === null) {
			return $this->setErrorMessage('Fehler', 'Fehler beim Hochladen der Datei oder es wurde keine Datei ausgewählt.');
		}

		$this->file = $file;
		$this->slugify = new Slugify();
		$this->setItemTemplate();
		$this->setSqliteDB();
		$this->handleFile();
	}

	private
	function setSqliteDB()
	{
		$docroot = realpath($_SERVER['DOCUMENT_ROOT']);
		$docroot = str_replace('public', '', $docroot);
		$this->sqliteDB = 'sqlite:' . $docroot . 'app' . DIRECTORY_SEPARATOR . 'database' . DIRECTORY_SEPARATOR . 'bolt.db';
	}

	private
	function handleFile()
	{
		$handle = fopen($this->file, "r");
		$lastIndex = '';
		if ($handle) {
			try {
				$i = 0;
				while (($line = fgets($handle)) !== false) {
					$l = str_replace(chr(0xAC) , "", str_replace(chr(0xC2) , "", str_replace("\t", "", rtrim(iconv('CP850', 'UTF-8', $line)))));
					if (strlen($l) == 0) {

						// save the last record

						if (!empty($this->item['_00']) && !isset($this->export[$this->item['_00']])) {
							$this->item['slug'] = $this->slugify->slugify($this->item['_00'] . '_' . $this->item['_20']);
							$this->item['title'] = $this->item['_20'];
							$this->export[$this->item['_00']] = $this->item;
							$i++;
						}

						$this->item = $this->item_template;
						$lastIndex = '';
					}
					elseif (substr($l, 0, 1) == '#' && strlen($l) >= 3) {
						switch (trim(substr($l, 1, 3))) {
						case '00':

							// save the last record

							if (!empty($this->item['_00']) && !isset($this->export[$this->item['_00']])) {
								$this->item['slug'] = $this->slugify->slugify($this->item['_00'] . '_' . $this->item['_20']);
								$this->item['title'] = $this->item['_20'];
								$this->export[$this->item['_00']] = $this->item;
								$i++;
							}

							$this->item = $this->item_template;
							$this->item['_00'] = substr($l, 4, 1000);
							$lastIndex = '_00';
							break;

						case '20':
							$this->item['_20'] = substr($l, 4, 1000);
							$lastIndex = '_20';
							break;

						case '23':
							$this->item['_23'] = substr($l, 4, 1000);
							$lastIndex = '_23';
							break;

						case '28':
							$this->item['_28'] = substr($l, 4, 1000);
							$lastIndex = '_28';
							break;

						case '30a':
							$this->item['_30a'] = substr($l, 4, 1000);
							$lastIndex = '_30a';
							break;

						case '31':
							$keywords = str_replace("; ", "\r\n", substr($l, 4, 1000));
							$keywords = str_replace(";", "\r\n", $keywords);
							$keywords = str_replace(chr(20) , "\r\n", $keywords);
							$this->item['_31'] = $keywords;
							$lastIndex = '_31';
							break;

						case '40':
							$this->item['_40'] = substr($l, 4, 1000);
							$lastIndex = '_40';
							break;

						case '40a':
							$this->item['_40a'] = substr($l, 4, 1000);
							$lastIndex = '_40a';
							break;

						case '40b':
							$this->item['_40b'] = substr($l, 4, 1000);
							$lastIndex = '_40b';
							break;

						case '40c':
							$this->item['_40c'] = substr($l, 4, 1000);
							$lastIndex = '_40c';
							break;

						case '41':
							$this->item['_41'] = substr($l, 4, 1000);
							$lastIndex = '_41';
							break;

						case '42':
							$this->item['_42'] = substr($l, 4, 1000);
							$lastIndex = '_42';
							break;

						case '50':
							$this->item['_50'] = substr($l, 4, 1000);
							$lastIndex = '_50';
							break;

						case '50f':
							$this->item['_50f'] = substr($l, 4, 1000);
							$lastIndex = '_50f';
							break;

						case '60':
							$this->item['_60'] = substr($l, 4, 1000);
							$lastIndex = '_60';
							break;

						case '61':
							$this->item['_61'] = substr($l, 4, 1000);
							$lastIndex = '_61';
							break;

						case '71':
							$this->item['_71'] = substr($l, 4, 1000);
							$lastIndex = '_71';
							break;

						case '74':
							$this->item['_74'] = substr($l, 4, 1000);
							$lastIndex = '_74';
							break;

						case '75':
							$this->item['_75'] = substr($l, 4, 1000);
							$lastIndex = '_75';
							break;

						case '76':
							$this->item['_76'] = substr($l, 4, 1000);
							$lastIndex = '_76';
							break;

						case '76p':
							$this->item['_76p'] = substr($l, 4, 1000);
							$lastIndex = '_76p';
							break;

						case '77':
							$this->item['_77'] = substr($l, 4, 1000);
							$lastIndex = '_77';
							break;

						case '80':
							$this->item['_80'] = substr($l, 4, 1000);
							$lastIndex = '_80';
							break;

						case '84':
							$this->item['_84'] = substr($l, 4, 1000);
							$lastIndex = '_84';
							break;

						case '85':
							$this->item['_85'] = substr($l, 4, 1000);
							$lastIndex = '_85';
							break;

						case '87':
							$this->item['_87'] = substr($l, 4, 1000);
							$lastIndex = '_87';
							break;

						case '88':
							$this->item['_88'] = substr($l, 4, 1000);
							$lastIndex = '_88';
							break;

						case '90':
							$this->item['_90'] = substr($l, 4, 1000);
							$lastIndex = '_90';
							break;

						case '99n':
							break;
						}
					}
					elseif (!empty($lastIndex)) {
						$this->item[$lastIndex].= $l;
					}
				}
			}

			catch(Exception $e) {
				return $this->setErrorMessage('Fehler beim Auslesen der Datensätze', null, $e->getMessage());
			}

			finally {
				fclose($handle);
			}

			if ($i > 0) {
				try {
					$link = new PDO($this->sqliteDB);
				}

				catch(Exception $e) {
					return $this->setErrorMessage('Kein Zugriff auf die Datenbank möglich', null, $e->getMessage());
				}

				$link->exec('pragma synchronous = off;');

				// start transaction - so nothing is lost if import would fail

				$link->beginTransaction();

				// empty database

				$sql = "DELETE FROM bolt_libraryentries;";
				if (!$link->query($sql)) {
					return $this->setErrorMessage('Fehler beim Entfernen der alten Datensätze', $link->errorInfo() [2]);
				}

				// insert new library entries

				$keys = array_keys(current($this->export));
				$keys_str = '`' . join('`, `', $keys) . '`';
				$val_placeholder = ':' . join(', :', $keys);
				$sql_tpl = "INSERT INTO bolt_libraryentries (\r\n";
				$sql_tpl.= "\t" . $keys_str . " )\r\nVALUES (\r\n\t";
				foreach($this->export as $this->item) {
					$sql = $sql_tpl;
					$values = [];
					foreach($keys as $k) {
						$val = $this->item[$k];
						if (is_string($val)) {
							$val = str_replace("'", "''", $val);
							$val = "'" . $val . "'";
						}

						if ($val === null) $val = 'NULL';
						$values[] = $val;
					}

					$sql.= join(', ', $values) . " ); ";
					if (!$link->query($sql)) {
						return $this->setErrorMessage('Fehler beim Import der Datensätze', $link->errorInfo() [2]);
					}
				}

				// commit transaction

				if (!$link->commit()) {
					return $this->setErrorMessage('Fehler beim Commit der Datensätze', $link->errorInfo() [2]);
				}

				// vacuum database

				$link->exec('VACUUM');

				// feedback for user

				return $this->setInfoMessage('Erfolgreich', $i . ' Einträge erfolgreich importiert!');
			}
			else {
				return $this->setErrorMessage('Keine Datensätze', 'In der Datei wurden keine Datensätze gefunden.<br />Ist die Datei wirklich ein Allegro-Export?');
			}
		}
		else {
			return $this->setErrorMessage('Fehler', 'Die Datei kann leider nicht geladen werden.');
		}
	}

	private
	function setMessage($title, $message = false, $detail = false)
	{
		$msg = '<strong>' . $title . '</strong>';
		if (is_string($message)) {
			$msg.= '<br /><br />' . $message;
		}

		if (is_string($detail)) {
			$msg.= '<br /><br /><small>' . $detail . '</small>';
		}

		$this->message = $msg;
	}

	private
	function setErrorMessage($title, $message = false, $detail = false)
	{
		$this->response = Response::HTTP_BAD_REQUEST;
		$this->setMessage($title, $message, $detail);
	}

	private
	function setInfoMessage($title, $message = false, $detail = false)
	{
		$this->response = Response::HTTP_OK;
		$this->setMessage($title, $message, $detail);
	}

	private
	function setItemTemplate()
	{
		$this->item_template = ["slug" => "bibliothekseintrag-", "datecreated" => date('Y-m-d H:i:s') , "datechanged" => date('Y-m-d H:i:s') , "datepublish" => date('Y-m-d H:i:s') , "datedepublish" => null, "username" => "", "ownerid" => 2, "status" => "published", "_00" => "", "_20" => "", "_23" => "", "_28" => "", "_30a" => "", "_31" => "", "_40" => "", "_40a" => "", "_40b" => "", "_40c" => "", "_41" => "", "_42" => "", "_50" => "", "_50f" => "", "_60" => "", "_61" => "", "_71" => "", "_74" => "", "_75" => "", "_76" => "", "_76p" => "", "_77" => "", "_80" => "", "_84" => "", "_85" => "", "_87" => "", "_88" => "", "_90" => "", "templatefields" => "[]"];
	}
}
